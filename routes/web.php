<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
$defaultRoutes = ['Jobs','Meetingstypes', 'Companies'];
Route::group(['prefix' => 'pages', 'as' => 'pages.'], function () use($defaultRoutes){
    foreach ($defaultRoutes as $route) {
        Route::resource('/'.strtolower($route), $route."Controller");
    }
    Route::match(['put', 'patch'], '/play/inventory/{id}', ['uses' => "PlayController@inventory", 'as' => "play.inventory"]);
});
foreach ($defaultRoutes as $route) {
    Route::post('/'.strtolower($route).'_mass_destroy', ['uses' => $route."Controller@massDestroy", 'as' => "pages.".strtolower($route).".mass_destroy"]);
}

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
