<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobs extends Model
{
    public $table = 'jobs';

    protected $fillable = [
        'id',
        'user_id',
        'company_id',
        'meeting_type_id',
        'meeting',
        'name',
        'created_at',
        'updated_at'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    private $fillableType = [];

    public function getFillable() {
        return $this->fillable;
    }

    public function getFillableType() {
        return $this->fillableType;
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function company() {
        return $this->belongsTo(Companies::class);
    }

    public function meetingType() {
        return $this->belongsTo(Meetingstypes::class);
    }
}
