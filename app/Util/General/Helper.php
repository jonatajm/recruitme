<?php

namespace App\Util\General;

use Illuminate\Support\Facades\Auth;

class Helper
{
    public static function setUserId(&$arrayRequestData) {
        $arrayRequestData['user_id'] = Auth::user()->id;
    }
}
