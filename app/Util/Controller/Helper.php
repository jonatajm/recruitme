<?php

namespace App\Util\Controller;

class Helper
{
    public static function getTemplate($template, $params) {
        return view($template, $params);
    }

    public static function getTableHeads($names) {
        $stringToReturn = "";
        if (!is_array($names)) {
            throw new \Exception('The column names must be given in array format');
        }
        foreach ($names as $name) {
            $stringToReturn .= "<th>{$name}</th>";
        }
        return $stringToReturn;
    }

    public static function getTableColumns($data, $names) {
        $stringToReturn = "";
        if (!is_array($names)) {
            throw new \Exception('The column names must be given in array format');
        }
        foreach ($names as $id => $name) {
            if (strpos($id, ".")) {
                $relationField = explode(".", $id);
                $stringToReturn .= "<td>".$data[$relationField[0]][$relationField[1]]."</td>";
            } else {
                $stringToReturn .= "<td>{$data->$id}</td>";
            }
        }
        return $stringToReturn;
    }
}
