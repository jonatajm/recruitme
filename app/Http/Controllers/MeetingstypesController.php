<?php

namespace App\Http\Controllers;

use App\Companies;
use App\Http\Requests\MeetingstypesStorageRequest;
use App\Http\Requests\MeetingstypesUpdateRequest;
use App\Jobs;
use App\Meetingstypes;
use App\Util\Controller\Helper;

class MeetingstypesController extends Controller
{
    private $classRouteKey = 'pages.meetingstypes';
    private $classControler = Meetingstypes::class;
    private $classTable = 'meetings_types';

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of Example.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classControler = $this->classControler;
        $routeKey = $this->classRouteKey;
        $tableHeadNames = ["name"=>"Name"];
        $query = $classControler::query();
        $datas = $query->select([
            $this->classTable.'.id',
            $this->classTable.'.user_id',
            $this->classTable.'.name',
        ])->get();
        $tableBody = "";
        if (count($datas) > 0) {
            $gateKey  = $this->classTable.'_';
            foreach ($datas as $data) {
                $tableBody .= "<tr>
                                <td><input type='checkbox' class='selectable' value='{$data->id}' name='checkBox[]' /></td>
                                ".Helper::getTableColumns($data, $tableHeadNames)."
                                <td>
                                    ".Helper::getTemplate('layouts.actionsTemplate', ['row'=>$data, 'gateKey'=>$gateKey, 'routeKey'=>$routeKey])."
                                </td>
                            </tr>";
            }
        } else {
            $tableBody .= "<tr>
                                <td colspan='99' style='text-align: center;'>Nothing to show</td>
                            </tr>";
        }
        $table = "<table id=\"tabelaImages\" class=\"table table-bordered table-hover tabelaDados\">
                        <thead>
                            <tr>
                                <th style='width: 40px;'><input type='checkbox' class='selecionarTodos' /></th>
                                ".Helper::getTableHeads($tableHeadNames)."
                                <th style='width: 15%;'>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {$tableBody}
                        </tbody>
                        <tfoot>
                            <tr>
                                <th style='width: 40px;'><input type='checkbox' class='selecionarTodos' /></th>
                                ".Helper::getTableHeads($tableHeadNames)."
                                <th>Actions</th>
                            </tr>
                        </tfoot>
                    </table>";
        return view($this->classRouteKey.'.index', compact('table','routeKey'));
    }

    /**
     * Show the form for creating new Example.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $routeKey = $this->classRouteKey;
        return view($this->classRouteKey.'.create', compact('routeKey'));
    }

    /**
     * Store a newly created Example in storage.
     *
     * @param  \App\Http\Requests\StoreProjectsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MeetingstypesStorageRequest$request)
    {
        $classControler = $this->classControler;
        $dataRequest = $request->all();
        \App\Util\General\Helper::setUserId($dataRequest);
        $classControler::create($dataRequest);

        return redirect()->route($this->classRouteKey.'.index');
    }


    /**
     * Show the form for editing Example.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $classControler = $this->classControler;
        $routeKey = $this->classRouteKey;
        $data = $classControler::findOrFail($id);
        return view($this->classRouteKey.'.edit', compact('data', 'routeKey'));
    }

    /**
     * Update Example in storage.
     *
     * @param  \App\Http\Requests\UpdateProjectsRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MeetingstypesUpdateRequest $request, $id)
    {
        $classControler = $this->classControler;
        $update = $classControler::findOrFail($id);
        $dataRequest = $request->all();
        \App\Util\General\Helper::setUserId($dataRequest);
        $update->update($dataRequest);

        return redirect()->route($this->classRouteKey.'.index');
    }


    /**
     * Display Example.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $classControler = $this->classControler;
        $routeKey = $this->classRouteKey;
        $data = $classControler::findOrFail($id);

        return view($this->classRouteKey.'.show', compact('data', 'routeKey'));
    }


    /**
     * Remove Example from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $classControler = $this->classControler;
        $delete = $classControler::findOrFail($id);
        $delete->delete();

        return redirect()->route($this->classRouteKey.'.index');
    }

    /**
     * Delete all selected Example at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        $classControler = $this->classControler;
        if ($request->input('ids')) {
            $entries = $classControler::whereIn('id', $request->input('ids'))->get();
            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }
}