<?php

namespace App\Http\Controllers;

use App\Companies;
use App\Jobs;
use App\Meetingstypes;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $qtdCompanies = Companies::all()->count();
        $qtdJobs = Jobs::all()->count();
        $qtdMeetingstypes = Meetingstypes::all()->count();
        return view('pages.home', compact('qtdCompanies', 'qtdJobs', 'qtdMeetingstypes'));
    }
}
