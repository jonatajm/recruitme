<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meetingstypes extends Model
{
    public $table = 'meetings_types';

    protected $fillable = [
        'id',
        'user_id',
        'name',
        'created_at',
        'updated_at'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    private $fillableType = [];

    public function getFillable() {
        return $this->fillable;
    }

    public function getFillableType() {
        return $this->fillableType;
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
