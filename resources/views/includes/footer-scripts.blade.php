<!-- jQuery -->
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('plugins/morris/morris.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
$.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<!-- Sparkline -->
<script src="{{asset('plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('plugins/knob/jquery.knob.js')}}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{asset('plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('plugins/fastclick/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('dist/js/pages/dashboard.js')}}"></script>

<script>

    $('.remove-file-custom').click(function() {
        var inputId = $(this).id;
        $(this).parent().find('a.imageLink').remove();
        $(this).parent().find('a.remove-file-custom').remove();
        if ($('#imagesToDelete').val().substr(0, $('#imagesToDelete').val().lenght) == "") {
            $('#imagesToDelete').val(inputId);
        } else {
            $('#imagesToDelete').val($('#imagesToDelete').val()+','+inputId);
        }
    });
    $('.selecionarTodos').click(function () {
        var thisSelecionarTodos = this;
        $('.selectable').each(function () {
            if (thisSelecionarTodos.checked == false) {
                $(this)[0].checked = false;
            } else {
                $(this)[0].checked = true;
            }
        });
        $('.selecionarTodos').each(function () {
            if (thisSelecionarTodos.checked == false) {
                $(this)[0].checked = false;
            } else {
                $(this)[0].checked = true;
            }
        });
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.massDestroy').click(function () {
        if (confirm('Are you sure that you whant to delete all selected rows?')) {
            var ids = [];
            $('.tabelaDados').find('.selectable').each(function () {
                if ($(this)[0].checked == true) {
                    ids.push($(this).val());
                }
            });
            $.ajax({
                method: 'POST',
                url: $(this).attr('href'),
                data: {
                    ids: ids
                }
            }).done(function () {
                location.reload();
            });
        }

        return false;
    });
    $(function () {
        $('.datepicker').datepicker();
    });
</script>