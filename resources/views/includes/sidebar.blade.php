<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
        <img src="{{asset('dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">RecruitME</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar" style="float: left;">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{ route('pages.jobs.index') }}"  class="nav-link">
                        <i class="nav-icon fa fa-file"></i>
                        <p>Jobs</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('pages.meetingstypes.index') }}"  class="nav-link">
                        <i class="nav-icon fa fa-gear"></i>
                        <p>Meetings types</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('pages.companies.index') }}"  class="nav-link">
                        <i class="nav-icon fa fa-building"></i>
                        <p>Companies</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>