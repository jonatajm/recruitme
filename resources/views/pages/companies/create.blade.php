@extends('layouts.default')
@section('content')
    <script type="text/javascript" src="job.js"></script>
    <div class="row" id="job">
        <div class="col-12">
            <div class="card">
                <div class="card-header row">
                    <h3 class="card-title" style="float: left; margin-left: 15px; line-height: 37px;">Add Company</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    {!! Form::open(['method' => 'POST', 'route' => [$routeKey.'.store']]) !!}
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 form-group">
                                    <div class="row">
                                        {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
                                        {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::submit('Save', ['class' => 'btn btn-danger']) !!}
                    <a href="{{ route($routeKey.'.index') }}" class="btn btn-default">Back</a>
                    {!! Form::close() !!}
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
@stop