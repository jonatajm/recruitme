@extends('layouts.default')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header row">
                    <h3 class="card-title" style="float: left; margin-left: 15px; line-height: 37px;">Show company</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="panel-body table-responsive">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <table class="table table-bordered table-striped">
                                                <tr>
                                                    <th>Name</th>
                                                    <td field-key='name'>{{ $data->name}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                    <a href="{{ route($routeKey.'.index') }}" class="btn btn-default">Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
@stop