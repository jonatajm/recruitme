@extends('layouts.default')
@section('content')
    <div class="row" id="job">
        <div class="col-12">
            <div class="card">
                <div class="card-header row">
                    <h3 class="card-title" style="float: left; margin-left: 15px; line-height: 37px;">Add job</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    {!! Form::open(['method' => 'POST', 'route' => [$routeKey.'.store']]) !!}
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 form-group">
                                    <div class="row">
                                        {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
                                        {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                                    </div>
                                    <div class="row">
                                        {!! Form::label('company_id', 'Company', ['class' => 'control-label']) !!}
                                        {!! Form::select('company_id', $companies, old('company_id'), ['class' => 'form-control select2', 'required' => '']) !!}
                                    </div>
                                    <div class="row">
                                        {!! Form::label('meeting_type_id', 'Meeting type', ['class' => 'control-label']) !!}
                                        {!! Form::select('meeting_type_id', $meetingsTypes, old('meeting_type_id'), ['class' => 'form-control select2', 'required' => '']) !!}
                                    </div>
                                    <div class="row">
                                        {!! Form::label('meeting', 'Meeting', ['class' => 'control-label']) !!}
                                        {!! Form::text('meeting_date', old('meeting_date'), ['class' => 'form-control datepicker', 'placeholder' => '']) !!}
                                        {!! Form::time('meeting_time', old('meeting_time'), ['class' => 'form-control', 'placeholder' => '']) !!}
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::submit('Save', ['class' => 'btn btn-danger']) !!}
                    <a href="{{ route($routeKey.'.index') }}" class="btn btn-default">Voltar</a>
                    {!! Form::close() !!}
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
@stop