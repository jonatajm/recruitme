@extends('layouts.default')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header row">
                    <h3 class="card-title" style="float: left; margin-left: 15px; line-height: 37px;">Show job</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="panel-body table-responsive">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <table class="table table-bordered table-striped">
                                                <tr>
                                                    <th>Name</th>
                                                    <td field-key='project_id'>{{ $data->name}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Company</th>
                                                    <td field-key='description'>{{ $data->company->name}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Meeting Type</th>
                                                    <td field-key='question'>{{ $data->meetingType->name}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Meeting</th>
                                                    <td field-key='question'>{{ $data->meeting}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                    <a href="{{ route($routeKey.'.index') }}" class="btn btn-default">Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
@stop