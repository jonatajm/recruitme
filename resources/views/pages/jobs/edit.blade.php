@extends('layouts.default')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header row">
                    <h3 class="card-title" style="float: left; margin-left: 15px; line-height: 37px;">Edit job</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    {!! Form::open(['method' => 'PUT', 'route' => [$routeKey.'.update', $data->id]]) !!}
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 form-group">
                                    <div class="row">
                                        {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
                                        {!! Form::text('name', $data->name, ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                                    </div>
                                    <div class="row">
                                        {!! Form::label('company_id', 'Company', ['class' => 'control-label']) !!}
                                        {!! Form::select('company_id', $companies, $data->company_id, ['class' => 'form-control select2', 'required' => '']) !!}
                                    </div>
                                    <div class="row">
                                        {!! Form::label('meeting_type_id', 'Meeting type', ['class' => 'control-label']) !!}
                                        {!! Form::select('meeting_type_id', $meetingsTypes, $data->meeting_type_id, ['class' => 'form-control select2', 'required' => '']) !!}
                                    </div>
                                    <div class="row">
                                        {!! Form::label('meeting', 'Meeting', ['class' => 'control-label']) !!}
                                        {!! Form::text('meeting_date', $meetingDate, ['class' => 'form-control datepicker', 'placeholder' => '']) !!}
                                        {!! Form::time('meeting_time', $meetingTime, ['class' => 'form-control', 'placeholder' => '']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::submit('Save', ['class' => 'btn btn-danger']) !!}
                    <a href="{{ route($routeKey.'.index') }}" class="btn btn-default">Back</a>
                    {!! Form::close() !!}
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
@stop