<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body class="sidebar-mini" id="app">
<div class="wrapper">
    @include('includes.header')
    @include('includes.sidebar')

    <div id="main" class="content-wrapper" style="min-height: 213px;">
        @yield('content')
    </div>

    <footer class="row">
        @include('includes.footer-scripts')
    </footer>

    <!-- /.content-wrapper -->
    <footer class="main-footer">
    </footer>
</div>
</body>
</html>