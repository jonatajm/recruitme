    <a href="{{ route($routeKey.'.show', $row->id) }}"
       class="btn btn-xs btn-primary"><i class="fa fa-eye"></i></a>
    <a href="{{ route($routeKey.'.edit', $row->id) }}" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i></a>
    {!! Form::open(array(
        'style' => 'display: inline-block;',
        'method' => 'DELETE',
        'onsubmit' => "return confirm('".trans("Are you sure?")."');",
        'route' => [$routeKey.'.destroy', $row->id])) !!}
    {!! Form::button('<i class="fa fa-trash"></i>', array('type' => 'submit', 'class' => 'btn btn-xs btn-danger')) !!}
    {!! Form::close() !!}
